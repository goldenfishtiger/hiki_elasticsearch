require 'cgi'

def esearch_form

	host = ENV['ELASTICSEARCH_SERVICE_HOST'] || 'elasticsearch'
	port = ENV['ELASTICSEARCH_SERVICE_PORT'] || 9200

	es = Elasticsearch.new('http://%s:%d' % [host, port])
	es.active or return('-- elasticsearch service not active --')

	params = @request.params
	title = params.keys[0]
	search_key = params['key']

	text = ''
#	text << "title: %s<BR>\n" % title
#	text << "params: %s<BR>\n" % params.inspect
#	text << "search_key: %s<BR>\n" % search_key

	# SEARCH
	body = {
		'query' => {
			'match' => {
				'text'	=> search_key,
			},
		},
	}
	res = es.cat_indices('kuromoji_index')
	total = res['kuromoji_index']['docs.count']

	http_res = es.post('/kuromoji_index/_search/', body)
	res = JSON.parse(http_res.body)

	if(res['hits'] and hits = res['hits']['hits'])

		text << "<UL>"
		hits.each {|hit|
			# hit['_source']['date']
#			text << "<LI><A href='./?cmd=view&p=%s&key=%s'>_%s</A> - [%s]</LI>\n" % [it = hit['_source']['title'], search_key, it, hit['_source']['text'][0, 64]]
			text << "<LI><A href='./?cmd=view&p=%s&key=%s'>%s</A> - [%s]</LI>\n" % [it = hit['_id'], search_key, CGI.unescape(it), hit['_source']['text'][0, 64]]
		}
		text << "</UL>"

		text << "Hits: %d/%d<BR>\n" % [hits.size, total]

	else
		text << '-- elasticsearch service error --'
	end

	text << <<EOF
	<FORM action='./' method='GET'>
		<input type='hidden' name='#{title}'>
		#{esearch_post_label}: <INPUT type='text' size='30' maxlength='50' name='key' value=''>
		<INPUT type='submit' value='#{esearch_post_label}'>
	</FORM>
EOF

#	text << '<PRE><CODE>' + JSON.pretty_generate(res).gsub(/ /, '_') + '</CODE></PRE>'	# debug

	text
end

__END__

