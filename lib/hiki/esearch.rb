#-------------------------------------------------------------------------------
#
#	Elasticsearch
#
require 'uri'
require 'net/http'
require 'json'

class Elasticsearch

	attr_reader :active

	def initialize(host = 'http://localhost:9200')
		@host = host
		begin
			@active = true
			get('/_cat/health/?v')
		rescue
			@active = false
		end
	end

	def cat_health
		res0 = get('/_cat/health/?v')
		hash_cat_result(res0, 2)
	end

	def cat_indices(index = nil)
		res0 = get('/_cat/indices%s/?v' % [(it = index) ? '/' + it : ''])
		hash_cat_result(res0, 2)
	end

	def cat_count(index = nil)
		res0 = get('/_cat/count%s/?v' % [(it = index) ? '/' + it : ''])
		hash_cat_result(res0)
	end

	def hash_cat_result(res0, key_nth = nil)
		dkey = '@'
		res0s = res0.body.split(/\n/)
		res = {}
		th = res0s[0].split(/\s+/)
		res0s[1..-1].each {|tr|
			vs = {}
			trs = tr.split(/\s+/)
			trs.each_index {|n|
				vs[th[n]] = (it = trs[n]) =~ /^\d+$/ ? it.to_i : it
			}
			res[(it = key_nth) ? trs[it] : dkey.succ!] = vs
		}
		res
	end

	def setup_kuromoji(index = 'kuromoji_index')
		body = {
			'settings' => {
				'analysis' => {
					'analyzer' => {
						'japanese_analyzer' => {
							'type'			=> 'custom',
							'char_filter'	=> ['icu_normalizer', 'kuromoji_iteration_mark'],
							'tokenizer'		=> 'kuromoji_tokenizer',
							'filter'		=> ['kuromoji_baseform', 'kuromoji_part_of_speech', 'ja_stop', 'kuromoji_number', 'kuromoji_stemmer'],
						},
					},
				},
			},
			'mappings' => {
				'properties' => {
					'text' => {
						'type'			=> 'text',
						'analyzer'		=> 'japanese_analyzer',
					},
				},
			},
		}
		put('/%s/' % index, body)
	end

	def get(path)
		url = URI.parse(@host + path)
		req = Net::HTTP::Get.new(url.request_uri)
		request(url, req)
	end

	def put(path, body)
		url = URI.parse(@host + path)
		req = Net::HTTP::Put.new(url.request_uri)
		req['Content-Type'] = 'application/json'
		req.body = JSON.pretty_generate(body)
		request(url, req)
	end

	def post(path, body)
		url = URI.parse(@host + path)
		req = Net::HTTP::Post.new(url.request_uri)
		req['Content-Type'] = 'application/json'
		req.body = JSON.pretty_generate(body)
		request(url, req)
	end

	def request(url, req)
#		p [url.host, url.port, url.path, url.query, url.request_uri]
		res = Net::HTTP.new(url.host, url.port).start {|http|
			http.request(req)
		}
	end
end

__END__

