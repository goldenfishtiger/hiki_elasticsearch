FROM docker.io/goldenfishtiger/hiki

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG TZ
ARG http_proxy
ARG https_proxy

# git clone hiki_elasticsearch しておくこと
COPY hiki_elasticsearch /home/user/hiki/
USER root
RUN set -x \
	&& /usr/bin/chown -R user:user /home/user
USER user

