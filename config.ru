#!/usr/bin/env rackup
# -*- ruby -*-

$VERBOSE = nil
Encoding.default_external = 'UTF-8'

pwd = `pwd`.chomp.split('/')
cdir = pwd[0..-1].join('/')
pdir = pwd[0..-2].join('/')
pdir.size == 0 and pdir = '/tmp'

unless(FileTest.exist?('%s/data/text' % pdir))
	puts('make data directory at [%s]' % pdir)
	system('/bin/mkdir -p %s/data' % pdir)
	system('/bin/cp -a %s/data/* %s/data'% [cdir, pdir])
end

unless(FileTest.exist?('%s/hikiconf.rb' % cdir))
	puts('make config file at [%s]' % cdir)
	system('/bin/cat %s/hikiconf.rb.sample | /bin/sed "s/^@data_path.*/@data_path       = \'%s\/data\'/" > %s/hikiconf.rb' % [cdir, pdir.gsub(/\//, '\/'), cdir])
end

$LOAD_PATH.unshift "lib"

require 'hiki/esearch'
require 'hiki/config'

host = ENV['ELASTICSEARCH_SERVICE_HOST'] || 'elasticsearch'
port = ENV['ELASTICSEARCH_SERVICE_PORT'] || 9200

es = Elasticsearch.new('http://%s:%d' % [host, port])
if(es.active)

	# setup kuromoji
	res = es.cat_indices
	puts(res.inspect)
	unless(res[it = 'kuromoji_index'])
		res = es.setup_kuromoji(it)
		puts(res.body)
	end

	conf = Hiki::Config.new
	db = conf.database
	db.page_info.each {|page_info|
		puts('page: %s' % page = page_info.keys[0])
		puts('  info: %s' % info = page_info[page])
		puts('  date: %s' % info[:last_modified])
		text = db.load(page)
		body = {
			'analyzer'	=> 'japanese_analyzer',
#			'title'		=> page,
#			'date'		=> info[:last_modified].to_s,
#			'keyword'	=> info[:keyword].join(' '),
			'text'		=> text,
		}
		etitle = CGI.escape(page)
		res = es.post('/kuromoji_index/_doc/%s/' % etitle, body)
#		res = es.post('/kuromoji_index/_analyze/', body)
		puts(JSON.pretty_generate(JSON.parse(res.body)))
	}

else
	puts('-- elasticsearch service not active --')
end

require "hiki/app"
require "hiki/attachment"

use Rack::Lint
use Rack::ShowExceptions
use Rack::Reloader
# use Rack::Session::Cookie
# use Rack::ShowStatus
use Rack::CommonLogger
use Rack::Static, :urls => ["/theme"], :root => "."

map "/" do
  run Hiki::App.new("hikiconf.rb")
end
map "/attach" do
  run Hiki::Attachment.new("hikiconf.rb")
end
